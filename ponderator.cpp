#include <iostream>
#include <string>
#include <unistd.h>
#include <limits>
#include <string.h>

typedef std::numeric_limits< double > dbl;

#define MAX_MOVES 256

struct Board
{
    double chance;
    int param[8];
    int val[8][8];
};

Board boards[MAX_MOVES];
int sort[MAX_MOVES];
int boardNum = 0;
int sortByProbability = 0;

void copyBoardContentTo(int from, int to)
{
    for(int arg=0;arg<8;++arg)
    {
        boards[to].param[arg] = boards[from].param[arg];
    }
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            boards[to].val[x][y] = boards[from].val[x][y];
        }
    }
}

void copyBoardTo(int from, int to)
{
    copyBoardContentTo(from, to);
    boards[to].chance = boards[from].chance;
}

void outputBoard(int i, double chance)
{
    std::cout << "board" << std::endl;//std::cerr << "board" << std::endl;
    std::cout << chance << " " << std::endl;
    for(int arg=0;arg<8;++arg)
    {
        std::cout << boards[i].param[arg] << " ";
        //std::cerr << boards[i].param[arg] << " ";
    }//std::cerr << std::endl;
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            std::cout << boards[i].val[x][y] << " ";
            //std::cerr << boards[i].val[x][y] << " ";
        }//std::cerr << std::endl;
    }
}

void sortBoards(void)
{
    for(int i=1;i<boardNum;++i)
    {
        int j;
        for(j=i;boards[i].chance>boards[sort[j-1]].chance&&j>0;--j)
        {
            sort[j] = sort[j-1];
        }
        sort[j] = i;
    }
}

void fusionBoards(int scenario1, int scenario2)
{ // scenario1 < scenario2
    double probabilityOfScenario1 = boards[scenario1].chance / (boards[scenario1].chance + boards[scenario2].chance); // knowing sce1 or sce2
    int randomInt = rand()%10000000;
    if(randomInt > ((int)(probabilityOfScenario1*10000000.0)))
    { // the scenario 2 wins
        copyBoardContentTo(scenario2, scenario1);
    }
    boards[scenario1].chance += boards[scenario2].chance; // in any case the combined scenario takes the sum of the two
    --boardNum;
    if(scenario2 != boardNum)
    { // the scenario2 memory is not relevant anymore
        copyBoardTo(boardNum, scenario2);
    }
}

int main(int argc, char *argv[])
{
    if(argc < 1)
    {
        return 1;
    }
    
    int opt;
    int max_scenarios = 128, opt_approachValue = 0;
    
    double approachValue;

    while ((opt = getopt(argc, argv, "m:sa:")) != -1)
    {
        switch (opt)
        {
            case 'm':
                max_scenarios = atoi(optarg);
                break;
            case 's':
                sortByProbability = 1;
                break;
            case 'a':
                opt_approachValue = 1;
                approachValue = atof(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s [-m max scenarii] [-n] name\n",
                        argv[0]);
                exit(EXIT_FAILURE);
        }
    }
    
    /*if (optind >= argc)
    {
        fprintf(stderr, "Expected argument after options\n");
        exit(EXIT_FAILURE);
    }*/
    
    /*if(argc >= 2 && strcmp(argv[1], "choose_one") == 0)
    {
        choose_one = 1;
    }*/
    
    for(int i=0;i<MAX_MOVES;++i)
    {
        sort[i] = i;
    }
    
    srand(time(NULL)+getpid());
    
while(1)
{
    std::string str;
    std::cin >> str; // "board"
    //std::cerr << str << std::endl;
    if(str.find("end") != std::string::npos)
    {
        break;
    }
    
    std::cout.precision(dbl::max_digits10);
    
    double probability;
    std::cin >> probability;//std::cerr << probability << std::endl;

    for(boardNum=0;boardNum<MAX_MOVES;++boardNum)
    {
        std::string str;
        std::cin >> str;
        //std::cerr << str << " ";
        if(str.find("end") != std::string::npos)
        {
            break;
        }
        
        for(int arg=0;arg<8;++arg)
        {
            int param;
            std::cin >> param;//std::cerr << param << " ";
            boards[boardNum].param[arg] = param;
        }
        for(int y=0;y<8;++y)
        {
            for(int x=0;x<8;++x)
            {
                int piece;
                std::cin >> piece;//std::cerr << piece << " ";
                boards[boardNum].val[x][y] = piece;
            }//std::cerr << std::endl;
        }
    }
    
    for(int i=0;i<boardNum;++i)
    { // PONDERATE THE POSSIBILITIES
        boards[i].chance = ((probability)*(1./((double)boardNum)));
    } // their sum has to be equal to the probability variable
    
    while(boardNum > 1 && boardNum>max_scenarios)
    { // fusion
        int scenario1 = rand()%boardNum;
        int scenario2 = (scenario1+(1+rand()%(boardNum-1)))%boardNum;
        
        if(scenario1 > scenario2)
        { // we want the index of scenario 1 to be lower than scenario 2 (just to make next step easier)
            int var = scenario1;
            scenario1 = scenario2;
            scenario2 = var;
        }
        //std::cerr << scenario1 << " " << scenario2 << std::endl;
        
        fusionBoards(scenario1, scenario2);
    }
    
    if(opt_approachValue)
    { // try to fusion to approach ponderation as close as possible to approachValue
        sortBoards();
        for(int i=boardNum-1;i>0;--i)
        {
            if(boards[i].chance < approachValue)
            {
                fusionBoards(i-1, i);
            }
        }
    }
    
    if(sortByProbability)
    {
        // sort
        sortBoards();
    }
    /*for(int i=0;i<boardNum;++i)
    {
        std::cerr << sort[i] << " " ;
    }std::cerr << std::endl;*/
    
    /*if(choose_one)
    {
        int printed = 0;
        while(!printed)
        {
            for(int i=0;i<boardNum;++i)
            {
                int randomInt = rand()%10000000;
                if(randomInt < ((int)(boards[i].chance*10000000.0)))
                {
                    outputBoard(sort[i], probability);
                    printed = 1;
                    break;
                }
            }
        }
    }*/
    
    for(int i=0;i<boardNum;++i)
    {
        outputBoard(sort[i], boards[sort[i]].chance);
    }
    
}

    std::cout << "end" << std::endl;
    
    return 0;
}