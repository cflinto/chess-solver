#include <iostream>
#include <string>
#include <unistd.h>

std::string pieceToString(int v)
{
    std::string ret = "  ";
    if((v&8) == 8 && v != -1)
    {
        ret[0] = '#';
    }
    else
    {
        ret[0] = ' ';
    }
    
    switch(v&7)
    {
        case 0:
            ret[1] = 'p';
            break;
        case 1:
            ret[1] = 'k';
            break;
        case 2:
            ret[1] = 'b';
            break;
        case 3:
            ret[1] = 'r';
            break;
        case 4:
            ret[1] = 'Q';
            break;
        case 5:
            ret[1] = 'K';
            break;
    }
    
    return ret;
}

int main(int argc, char *argv[])
{
    if(argc != 1)
    {
        return 1;
    }

    int param;
    for(int i=0;i<8;++i)
    {
        std::cin >> param;
    }
    
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            std::cout << "|    ";
        }
        std::cout << std::endl;
        for(int x=0;x<8;++x)
        {
            int num;
            std::cin >> num;
            std::cout << "| " << pieceToString(num) << " ";
        }
        std::cout << std::endl;
        for(int x=0;x<8;++x)
        {
            std::cout << "|____";
        }
        std::cout << std::endl;
    }
    
    return 0;
}