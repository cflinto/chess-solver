#include <iostream>
#include <string>
#include <unistd.h>
#include <limits>
#include <string.h>

typedef std::numeric_limits< double > dbl;

#define MAX_MOVES 256

struct Board
{
    double chance;
    int param[8];
    int val[8][8];
};

Board board;

int main(int argc, char *argv[])
{
    if(argc < 1)
    {
        return 1;
    }
    
    double whiteWins=0.;
    double blackWins=0.;
    
while(1)
{
    std::string str;
    std::cin >> str; // "board"
    if(str.find("end") != std::string::npos)
    {
        break;
    }
    
    std::cout.precision(dbl::max_digits10);
    
    std::cin >> board.chance;


    for(int arg=0;arg<8;++arg)
    {
        int param;
        std::cin >> param;
        board.param[arg] = param;
    }
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            int piece;
            std::cin >> piece;
            board.val[x][y] = piece;
        }
    }

    std::cin >> str;
    if(str.find("end") == std::string::npos)
    {
        std::cerr << "error sum ponderation (one configuration has other than 1 possibilities)" << std::endl;
        exit(1);
    }
    
    if(board.param[5] == 0)
    {
        blackWins += board.chance;
    }
    if(board.param[6] == 0)
    {
        whiteWins += board.chance;
    }
    
}

    std::cout << whiteWins << " " << blackWins << std::endl;
    
    return 0;
}