/*#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[])
{
    if(argc != 1)
    {
        return 1;
    }
    
    int pipeToPlay[2], pipeFromPlay[2];
    
    if(pipe(pipeToPlay) == -1)
    {
        std::cerr << "Unable to open pipe" << std::endl;
        return 1;
    }
    
    if(pipe(pipeFromPlay) == -1)
    {
        std::cerr << "Unable to open pipe" << std::endl;
        return 1;
    }
    
    switch(fork())
    {
        case -1:
            std::cerr << "Unable to fork" << std::endl;
            exit(1);
        case 0:
            execl("./rules | ./ponderate choose_one", "");
            std::cerr << "Unable to exec" << std::endl;
            exit(1);
        default:
            ;
    }
    
    return 0;
}*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include <limits>
#include <string.h>

#define coord(x,y) ((y)*8+(x))

int board[64];
FILE *streamIn;
FILE *streamOut;
int turn, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, whiteWins, blackWins, lastPawnMoveOrTake;

int gameCounter = 0;

void outputBoard(void)
{
    fprintf(streamOut, "%d %d %d %d %d %d %d %d\n", turn, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, whiteWins, blackWins, lastPawnMoveOrTake);
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            fprintf(streamOut, "%d ", board[coord(x,y)]);
        }
        fprintf(streamOut, "\n");
    }
}

int main(int argc, char *argv[])
{
    
    char buf[256] = "";
    int pipeToChoose[2], pipeFromChoose[2];

    if(pipe(pipeToChoose) == -1)
    {
        std::cerr << "Unable to open pipe" << std::endl;
        return 1;
    }
    
    if(pipe(pipeFromChoose) == -1)
    {
        std::cerr << "Unable to open pipe" << std::endl;
        return 1;
    }
    switch(fork()){
        case -1:
            perror("fork");
            return -1;
        case 0:
            // child
            close(pipeFromChoose[0]);
            if(dup2(pipeFromChoose[1], 1) == -1)
            {
                std::cerr << "Unable to dup2" << std::endl;
                exit(1);
            }
            close(pipeFromChoose[1]);
            
            close(pipeToChoose[1]);
            if(dup2(pipeToChoose[0], 0) == -1)
            {
                std::cerr << "Unable to dup2" << std::endl;
                exit(1);
            }
            close(pipeToChoose[0]);
            execl("./play_one", "./play_one", NULL);
            //execl("/usr/bin/rev", "/usr/bin/rev", NULL);
            std::cerr << "Unable to execute" << std::endl;
        default:
            // parent
            ;
    }
    
    FILE *pipeOut = fdopen(pipeToChoose[1], "w");
    FILE *pipeIn = fdopen(pipeFromChoose[0], "r");
    streamIn = stdin;
    int isStdin = 1;
    streamOut = pipeOut;
    
    int n;
    close(pipeToChoose[0]);
    close(pipeFromChoose[1]);
    
while(1)
{
    fflush(pipeOut);
    std::string str;
    if(isStdin)
    {
        fscanf(streamIn, "%s", buf);
        str = std::string(buf);
        if(str.find("end") != std::string::npos)
        {
            break;
        }
    }
    else
    {
        fscanf(streamIn, "%s", buf);
        //str = std::string(buf);
        /*if(str.find("end") != std::string::npos)
        {
            fscanf(streamIn, "%s", buf);
            str = std::string(buf);
        }*/
    }

    double probability;
    fscanf(streamIn, "%s", &buf);
    probability = std::stof(buf);
    
    for(int i=0;i<8;++i)
    {
        switch(i)
        {
            case 0:
                fscanf(streamIn, "%d", &turn);
                break;
            case 1:
                fscanf(streamIn, "%d", &whiteCanCastleL);
                break;
            case 2:
                fscanf(streamIn, "%d", &whiteCanCastleR);
                break;
            case 3:
                fscanf(streamIn, "%d", &blackCanCastleL);
                break;
            case 4:
                fscanf(streamIn, "%d", &blackCanCastleR);
                break;
            case 5:
                fscanf(streamIn, "%d", &whiteWins);
                break;
            case 6:
                fscanf(streamIn, "%d", &blackWins);
                break;
            case 7:
                fscanf(streamIn, "%d", &lastPawnMoveOrTake);
                break;
            default:
                ;
        }
    }
    
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            fscanf(streamIn, "%d", &board[coord(x,y)]);
        }
    }
    
    streamIn = pipeIn;
    isStdin = 0;
    
    if(whiteWins || blackWins)
    {
        streamOut = stdout;
        fprintf(streamOut, "board\n%.32f\n", probability);
        outputBoard();
        fprintf(streamOut, "end\n");
        streamOut = pipeOut;
        streamIn = stdin;
        isStdin = 1;
        
        if(gameCounter%10 == 0)
        {
            std::cerr << "Finished game number " << gameCounter << std::endl;
        }
        ++gameCounter;
        
        continue;
    }
    
    fprintf(streamOut, "board\n%.32f\n", probability);
    outputBoard();
    //fprintf(streamOut, "end\n");
}

    std::cout << "end" << std::endl;
    fprintf(pipeOut, "end\n");
    
    fclose(streamOut);
    fclose(streamIn);
    
    wait(0);
    
    return 0;
}
