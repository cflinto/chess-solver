#include <iostream>
#include <string>
#include <unistd.h>

#define MAX_MOVES 256

struct Board
{
    int param[8];
    int val[8][8];
};

Board boards[MAX_MOVES];
int boardNum = 0;

void outputAndExit(int i)
{
    for(int arg=0;arg<8;++arg)
    {
        std::cout << boards[i].param[arg] << "\t";
    }
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            std::cout << boards[i].val[x][y] << "\t";
        }
    }
    exit(0);
}

int main(int argc, char *argv[])
{
    if(argc != 1)
    {
        return 1;
    }
    
    srand(time(NULL)+getpid());

    for(boardNum=0;boardNum<MAX_MOVES;++boardNum)
    {
        std::string str;
        std::cin >> str;
        if(str.find("end") != std::string::npos)
        {
            break;
        }
        double chance;
        std::cin >> chance;
        for(int arg=0;arg<8;++arg)
        {
            int param;
            std::cin >> param;
            boards[boardNum].param[arg] = param;
        }
        for(int y=0;y<8;++y)
        {
            for(int x=0;x<8;++x)
            {
                int piece;
                std::cin >> piece;
                boards[boardNum].val[x][y] = piece;
            }
        }
        int randomInt = rand()%10000000;
        if(randomInt < ((int)(chance*10000000.0)))
        {
            outputAndExit(boardNum);
        }
    }
    
    outputAndExit(rand()%boardNum);
    
    return 0;
}