#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include <limits>
#include <string.h>

int main(int argc, char *argv[])
{
    
    char buf[256] = "HELLO WORLD!";
    int mypipe[2];

    if(pipe(mypipe) == -1)
    {
        std::cerr << "Unable to open pipe" << std::endl;
        return 1;
    }
    
    switch(fork())
    {
        case -1:
            perror("fork");
            return -1;
        case 0:
            // child
            close(mypipe[0]);
            if(dup2(mypipe[1], 1) == -1)
            {
                std::cerr << "Unable to dup2" << std::endl;
                exit(1);
            }
            close(mypipe[1]);
            
            execl("./rules", "./rules", NULL);
            std::cerr << "Unable to execute" << std::endl;
        default:
            // parent
            close(mypipe[1]);
            if(dup2(mypipe[0], 0) == -1)
            {
                std::cerr << "Unable to dup2" << std::endl;
                exit(1);
            }
            close(mypipe[0]);
            
            execl("./ponderate", "./ponderate", "-m", "1", NULL);
            std::cerr << "Unable to execute" << std::endl;
    }
    
    wait(0);
    
    std::cerr << "play_one finished" << std::endl;
    
    return 0;
}
