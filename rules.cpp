#include <iostream>
#include <string>

#define coord(x,y) ((y)*8+(x))

#define PAWN 0
#define KNIGHT 1
#define BISHOP 2
#define ROOK 3
#define QUEEN 4
#define KING 5

#define VOID -1
#define WHITE 0
#define BLACK 8

#define PAWN_THAT_MOVED_BY_TWO_SHIFT 4
#define PAWN_THAT_MOVED_BY_TWO (1<<PAWN_THAT_MOVED_BY_TWO_SHIFT)

enum directionMoveType
{
    ALL, VERTICAL, DIAGONAL_SLASH, DIAGONAL_ANTISLASH, HORIZONTAL
};

struct restrictedMove
{
    int threatX; // allowed to  move to this case
    int threatY;
    directionMoveType direction;
};
    
int width;
int height;
int variableNum;
int whiteCanCastleR = 1;
int whiteCanCastleL = 1;
int blackCanCastleR = 1;
int blackCanCastleL = 1;
int possibleMoves = 0;
int lastPawnMoveOrTake = 0;
int turn = 0;
int isEchec = 0;
float probability;
int board[64];
int allowedMoves[64];
restrictedMove restrictedMoves[64];

int absInt(int v)
{
    if(v<0)
    {
        return -v;
    }
    return v;
}

int onBoard(int x, int y)
{
    return x>=0 && y>=0 && x<width && y<height;
}

int isPiece(int x, int y)
{
    return board[coord(x,y)] != VOID;
}

void reinitRestrictedAndAllowedMoves(void)
{
    for(int y=0;y<8;++y)
    {
        for(int x=0;x<8;++x)
        {
            allowedMoves[coord(x,y)] = 1;
            restrictedMoves[coord(x,y)].threatX = -1;
            restrictedMoves[coord(x,y)].threatY = -1;
            restrictedMoves[coord(x,y)].direction = ALL;
        }
    }
}

void printMove(int oldX, int oldY, int newX, int newY, int movedByTwo, int killX, int killY, int pieceTransformation, int wcl, int wcr, int bcl, int bcr, int ox2, int oy2, int nx2, int ny2, int whiteWins, int blackWins, int lt)
{//std::cerr << "moving (" << oldX << ", " << oldY << ") to (" << newX << ", " << newY << ")" << std::endl;
    if(restrictedMoves[coord(oldX, oldY)].direction != ALL)
    {
        if(newX != restrictedMoves[coord(oldX, oldY)].threatX || newY != restrictedMoves[coord(oldX, oldY)].threatY)
        {
            switch(restrictedMoves[coord(oldX, oldY)].direction)
            {
                case VERTICAL:
                    if(oldX != newX)
                    {
                        return;
                    }
                    break;
                case HORIZONTAL:
                    if(oldY != newY)
                    {
                        return;
                    }
                    break;
                case DIAGONAL_SLASH:
                    if(oldX+oldY != newX+newY)
                    {
                        return;
                    }
                    break;
                case DIAGONAL_ANTISLASH:
                    if(oldX-oldY != newX-newY)
                    {
                        return;
                    }
                    break;
                default:
                    std::cerr << "Unhandled direction" << std::endl;
                    return;
            }
        }
    }

    ++possibleMoves;
    std::cout << "move" << std::endl;
    /*int whiteWins = 0;
    int blackWins = 0;
    if((board[coord(newX, newY)]&7) == KING)
    {
        if((board[coord(newX, newY)]&8) == WHITE)
        {
            blackWins = 1;
        }
        else
        {
            whiteWins = 1;
        }
    }
    if(isDraw)
    {
        blackWins = 1;
        whiteWins = 1;
    }*/
    std::cout << (!turn) << "\t" << wcl << "\t" << wcr << "\t" << bcl << "\t" << bcr << "\t" << whiteWins << "\t" << blackWins << "\t" << lt << std::endl;
    for(int y=0;y<height;++y)
    {
        for(int x=0;x<width;++x)
        {
            if(x == newX && y == newY)
            {
                if(pieceTransformation == -1)
                {
                    std::cout << ((board[coord(oldX,oldY)]|(movedByTwo<<PAWN_THAT_MOVED_BY_TWO_SHIFT))&31) << "\t";
                }
                else
                {
                    std::cout << pieceTransformation << "\t";
                }
                continue;
            }
            if(x == nx2 && y == ny2)
            {
                if(pieceTransformation == -1)
                {
                    std::cout << ((board[coord(ox2,oy2)]|(movedByTwo<<PAWN_THAT_MOVED_BY_TWO_SHIFT))&31) << "\t";
                }
                else
                {
                    std::cout << pieceTransformation << "\t";
                }
                continue;
            }
            if(x == oldX && y == oldY)
            {
                std::cout << VOID << "\t";
                continue;
            }
            if(x == ox2 && y == oy2)
            {
                std::cout << VOID << "\t";
                continue;
            }
            if(x == killX && y == killY)
            {
                std::cout << VOID << "\t";
                continue;
            }
            if(isPiece(x,  y))
            {
                std::cout << (board[coord(x,y)]&15) << "\t";
            }
            else
            {
                std::cout << VOID << "\t";
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int isThreatened(int x, int y, int updateAllowed)
{
    int ret = 0;
    int diffX, diffY;
    for(diffX=-1,diffY=-1;onBoard(x+diffX, y+diffY);--diffX,--diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        ++diffX;++diffY;
                    }while(diffX != 0 || diffY != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=-1,diffY=1;onBoard(x+diffX, y+diffY);--diffX,++diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        ++diffX;--diffY;
                    }while(diffX != 0 || diffY != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=1,diffY=1;onBoard(x+diffX, y+diffY);++diffX,++diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        --diffX;--diffY;
                    }while(diffX != 0 || diffY != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=1,diffY=-1;onBoard(x+diffX, y+diffY);++diffX,--diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        --diffX;++diffY;
                    }while(diffX != 0 || diffY != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=1,diffY=0;onBoard(x+diffX, y+diffY);++diffX)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        --diffX;
                    }while(diffX != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=-1,diffY=0;onBoard(x+diffX, y+diffY);--diffX)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        ++diffX;
                    }while(diffX != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=0,diffY=1;onBoard(x+diffX, y+diffY);++diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        --diffY;
                    }while(diffY != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    for(diffX=0,diffY=-1;onBoard(x+diffX, y+diffY);--diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    do
                    {
                        allowedMoves[coord(x+diffX, y+diffY)] = 0;
                        ++diffY;
                    }while(diffY != 0);
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
            break;
        }
    }
    
    // knights
    for(diffX=-2;diffX<=2;++diffX)
    {
        if(diffX==0)
        {
            continue;
        }
        for(diffY=-2;diffY<=2;++diffY)
        {
            if(diffY==0)
            {
                continue;
            }
            if(absInt(diffX) == absInt(diffY))
            {
                continue;
            }
            int piece = board[coord(x+diffX, y+diffY)]&7;
            if(onBoard(x+diffX, y+diffY))
            {
                if(isPiece(x+diffX, y+diffY) && (piece==KNIGHT))
                {
                    if((board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
                    {
                        if(updateAllowed)
                        {
                            allowedMoves[coord(x+diffX, y+diffY)] = 0;
                            ret = 1;
                        }
                        else
                        {
                            return 1;
                        }
                    }
                }
            }
        }
    }
    
    // pawns
    if(onBoard(x-1, y+(turn*2-1)) && isPiece(x-1, y+(turn*2-1)) && ((board[coord(x-1, y+(turn*2-1))]&7) == PAWN) && ((board[coord(x-1,y+(turn*2-1))]&8) != (turn<<3)))
    {
        if(updateAllowed)
        {
            allowedMoves[coord(x-1, y+(turn*2-1))] = 0;
            ret = 1;
        }
        else
        {
            return 1;
        }
    }
    if(onBoard(x+1, y+(turn*2-1)) && isPiece(x+1, y+(turn*2-1)) && ((board[coord(x+1, y+(turn*2-1))]&7) == PAWN) && ((board[coord(x+1,y+(turn*2-1))]&8) != (turn<<3)))
    {
        if(updateAllowed)
        {
            allowedMoves[coord(x+1, y+(turn*2-1))] = 0;
            ret = 1;
        }
        else
        {
            return 1;
        }
    }
    
    for(diffX=-1;diffX<=1;++diffX)
    {
        for(diffY=-1;diffY<=1;++diffY)
        {
            if(!onBoard(x+diffX, y+diffY))
            {
                continue;
            }
            if(!isPiece(x+diffX, y+diffY))
            {
                continue;
            }
            if((board[coord(x+diffX, y+diffY)]&7) == KING && (board[coord(x+diffX, y+diffY)]&8) != (turn<<3))
            {
                if(updateAllowed)
                {
                    ret = 1;
                }
                else
                {
                    return 1;
                }
            }
        }
    }
    
    if(ret && updateAllowed)
    {
        for(int y=0;y<8;++y)
        {
            for(int x=0;x<8;++x)
            {
                allowedMoves[coord(x,y)] = !allowedMoves[coord(x,y)];
            }
        }
    }
    
    return ret;
}

int computeRestrictedMoves(int x, int y)
{
    int diffX, diffY;
    int threatX, threatY;
    int pieceCount;
    for(diffX=-1,diffY=-1,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;--diffX,--diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = DIAGONAL_ANTISLASH;
                    ++diffX;++diffY;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=-1,diffY=1,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;--diffX,++diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = DIAGONAL_SLASH;
                    ++diffX;--diffY;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=1,diffY=1,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;++diffX,++diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = DIAGONAL_ANTISLASH;
                    --diffX;--diffY;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=1,diffY=-1,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;++diffX,--diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==BISHOP || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = DIAGONAL_SLASH;
                    --diffX;++diffY;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=1,diffY=0,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;++diffX)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = HORIZONTAL;
                    --diffX;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=-1,diffY=0,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;--diffX)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = HORIZONTAL;
                    ++diffX;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=0,diffY=1,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;++diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = VERTICAL;
                    --diffY;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
    for(diffX=0,diffY=-1,pieceCount=0;onBoard(x+diffX, y+diffY)&&pieceCount<2;--diffY)
    {
        int piece = board[coord(x+diffX, y+diffY)]&7;
        if(isPiece(x+diffX, y+diffY))
        {
            if((piece==ROOK || piece==QUEEN) && (board[coord(x+diffX,y+diffY)]&8) != (turn<<3))
            {
                if(pieceCount < 1)
                {
                    break;
                }
                threatX = x+diffX;
                threatY = y+diffY;
                do
                {
                    restrictedMoves[coord(x+diffX, y+diffY)].threatX = threatX;
                    restrictedMoves[coord(x+diffX, y+diffY)].threatY = threatY;
                    restrictedMoves[coord(x+diffX, y+diffY)].direction = VERTICAL;
                    ++diffY;
                }while(diffX != 0 || diffY != 0);
                break;
            }
            else
            {
                ++pieceCount;
                continue;
            }
        }
    }
}

int tilesAreVoid(int xMin, int xMax, int y)
{
    for(int x=xMin;x<=xMax;++x)
    {
        if(isPiece(x, y))
        {
            return 0;
        }
    }
    return 1;
}

int tilesAreNotThreatened(int xMin, int xMax, int y)
{
    for(int x=xMin;x<=xMax;++x)
    {
        if(isThreatened(x, y, 0))
        {
            return 0;
        }
    }
    return 1;
}

int main(int argc, char *argv[])
{
    if(argc != 1)
    {
        return 1;
    }

    /*width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);
    variableNum = std::stoi(argv[3]);*/
    width = 8;
    height = 8;
    variableNum = 8;
    
while(1)
{
    possibleMoves = 0;
    
    std::string str;
    std::cin >> str;
    if(str.find("end") != std::string::npos)
    {
        break;
    }
    
    reinitRestrictedAndAllowedMoves();

    std::cin >> probability;
    
    int whiteWins = 0, blackWins = 0;
    
    for(int i=0;i<variableNum;++i)
    {
        switch(i)
        {
            case 0:
                std::cin >> turn;
                break;
            case 1:
                std::cin >> whiteCanCastleL;
                break;
            case 2:
                std::cin >> whiteCanCastleR;
                break;
            case 3:
                std::cin >> blackCanCastleL;
                break;
            case 4:
                std::cin >> blackCanCastleR;
                break;
            case 5:
                std::cin >> whiteWins;
                break;
            case 6:
                std::cin >> blackWins;
                break;
            case 7:
                std::cin >> lastPawnMoveOrTake;
                break;
            default:
                ;
        }
    }
    
    for(int y=0;y<height;++y)
    {
        for(int x=0;x<width;++x)
        {
            std::string str;
            std::cin >> str;
            board[coord(x,y)] = std::stoi(str);
        }
    }
    
    std::cout << "board" << std::endl;
    std::cout << probability << std::endl;
    
    if(whiteWins || blackWins)
    {
        //reinitRestrictedAndAllowedMoves(); // probably useless
        printMove(-1, -1, -1, -1, 0, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1, whiteWins, blackWins, lastPawnMoveOrTake);
        std::cout << "end" << std::endl;
        continue;
    }
    
    //int echecX = -1, echecY = -1;
    
    for(int y=0;y<height;++y)
    {
        for(int x=0;x<width;++x)
        {
            if(!isPiece(x, y))
            {
                continue;
            }
            int piece = board[coord(x, y)]&7;
            if(piece == KING && ((board[coord(x,y)]&8) == (turn<<3)))
            {
                isEchec = isThreatened(x, y, 1);
                computeRestrictedMoves(x, y);
                /*echecX = x;
                echecY = y;*/
            }
        }
    }
    
    if(lastPawnMoveOrTake >= 100)
    { // draw
        printMove(-1, -1, -1, -1, 0, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1, 1, 1, lastPawnMoveOrTake+1);
        std::cout << "end" << std::endl;
        continue;
    }
    
    for(int y=0;y<height;++y)
    {
        for(int x=0;x<width;++x)
        {
            if(!isPiece(x, y))
            {
                continue;
            }
            int piece = board[coord(x, y)]&7;
            int team = ((board[coord(x, y)]&BLACK)>>3);
            if(team != turn)
            {
                continue;
            }
            int diffX, diffY;
            int wcl = whiteCanCastleL, wcr = whiteCanCastleR, bcl = blackCanCastleL, bcr = blackCanCastleR;
            switch(piece)
            {
                case PAWN:
                    if(board[coord(x,y+(team*2-1))] == VOID && allowedMoves[coord(x, y+(team*2-1))])
                    { // pawn advance
                        if(y+(team*2-1) == 0 || y+(team*2-1) == 7)
                        {
                            printMove(x, y, x, y+(team*2-1), 0, -1, -1, QUEEN|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x, y+(team*2-1), 0, -1, -1, ROOK|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x, y+(team*2-1), 0, -1, -1, BISHOP|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x, y+(team*2-1), 0, -1, -1, KNIGHT|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                        }
                        else
                        {
                            printMove(x, y, x, y+(team*2-1), 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                        }
                    }
                    
                    // eat
                    if(onBoard(x-1, y+(team*2-1)) && isPiece(x-1, y+(team*2-1)) && ((board[coord(x-1,y+(team*2-1))]&8) != (team<<3)) && allowedMoves[coord(x-1, y+(team*2-1))])
                    {
                        if(y+(team*2-1) == 0 || y+(team*2-1) == 7)
                        {
                            printMove(x, y, x-1, y+(team*2-1), 0, -1, -1, QUEEN|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x-1, y+(team*2-1), 0, -1, -1, ROOK|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x-1, y+(team*2-1), 0, -1, -1, BISHOP|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x-1, y+(team*2-1), 0, -1, -1, KNIGHT|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                        }
                        else
                        {
                            printMove(x, y, x-1, y+(team*2-1), 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                        }
                    }
                    if(onBoard(x+1, y+(team*2-1)) && isPiece(x+1, y+(team*2-1)) && ((board[coord(x+1,y+(team*2-1))]&8) != (team<<3)) && allowedMoves[coord(x+1, y+(team*2-1))])
                    {
                        if(y+(team*2-1) == 0 || y+(team*2-1) == 7)
                        {
                            printMove(x, y, x+1, y+(team*2-1), 0, -1, -1, QUEEN|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x+1, y+(team*2-1), 0, -1, -1, ROOK|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x+1, y+(team*2-1), 0, -1, -1, BISHOP|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            printMove(x, y, x+1, y+(team*2-1), 0, -1, -1, KNIGHT|(team<<3), whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                        }
                        else
                        {
                            printMove(x, y, x+1, y+(team*2-1), 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                        }
                    }
                    // eat moving pawn
                    if(onBoard(x-1, y+(team*2-1)) && isPiece(x-1, y) && ((board[coord(x-1,y+(team*2-1))]&8) != (team<<3)) && ((board[coord(x-1,y)]&PAWN_THAT_MOVED_BY_TWO) != 0) && allowedMoves[coord(x-1, y+(team*2-1))])
                    {
                        printMove(x, y, x-1, y+(team*2-1), 0, x-1, y, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                    }
                    if(onBoard(x+1, y+(team*2-1)) && isPiece(x+1, y) && ((board[coord(x+1,y+(team*2-1))]&8) != (team<<3)) && ((board[coord(x+1,y)]&PAWN_THAT_MOVED_BY_TWO) != 0) && allowedMoves[coord(x+1, y+(team*2-1))])
                    {
                        printMove(x, y, x+1, y+(team*2-1), 0, x+1, y, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                    }
                    
                    if(y == 6 && team == 0 && board[coord(x,4)] == VOID && board[coord(x,5)] == VOID && allowedMoves[coord(x, 4)])
                    {
                        printMove(x, y, x, 4, 1, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                    }
                    if(y == 1 && team == 1 && board[coord(x,3)] == VOID && board[coord(x,2)] == VOID && allowedMoves[coord(x, 3)])
                    {
                        printMove(x, y, x, 3, 1, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                    }
                    break;
                case KNIGHT:
                    for(diffX=-2;diffX<=2;++diffX)
                    {
                        if(diffX==0)
                        {
                            continue;
                        }
                        for(diffY=-2;diffY<=2;++diffY)
                        {
                            if(diffY==0)
                            {
                                continue;
                            }
                            if(absInt(diffX) == absInt(diffY))
                            {
                                continue;
                            }
                            if(onBoard(x+diffX, y+diffY))
                            {
                                if(board[coord(x+diffX, y+diffY)] == VOID && allowedMoves[coord(x+diffX, y+diffY)])
                                {
                                    printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                                }
                                else if((board[coord(x+diffX, y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                                { // enemy
                                    printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                                }
                            }
                        }
                    }
                    break;
                case BISHOP:
                    for(diffX=-1,diffY=-1;onBoard(x+diffX, y+diffY);--diffX,--diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=-1,diffY=1;onBoard(x+diffX, y+diffY);--diffX,++diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=1,diffY=1;onBoard(x+diffX, y+diffY);++diffX,++diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=1,diffY=-1;onBoard(x+diffX, y+diffY);++diffX,--diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    break;
                case ROOK:
                    if(x==0 && y==0)
                    {
                        bcl = 0;
                    }
                    if(x==0 && y==7)
                    {
                        wcl = 0;
                    }
                    if(x==7 && y==0)
                    {
                        bcr = 0;
                    }
                    if(x==7 && y==7)
                    {
                        wcr = 0;
                    }
                    for(diffX=1,diffY=0;onBoard(x+diffX, y+diffY);++diffX)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=-1,diffY=0;onBoard(x+diffX, y+diffY);--diffX)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=0,diffY=1;onBoard(x+diffX, y+diffY);++diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=0,diffY=-1;onBoard(x+diffX, y+diffY);--diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    break;
                case QUEEN:
                    for(diffX=-1,diffY=-1;onBoard(x+diffX, y+diffY);--diffX,--diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=-1,diffY=1;onBoard(x+diffX, y+diffY);--diffX,++diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=1,diffY=1;onBoard(x+diffX, y+diffY);++diffX,++diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=1,diffY=-1;onBoard(x+diffX, y+diffY);++diffX,--diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=1,diffY=0;onBoard(x+diffX, y+diffY);++diffX)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=-1,diffY=0;onBoard(x+diffX, y+diffY);--diffX)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=0,diffY=1;onBoard(x+diffX, y+diffY);++diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    for(diffX=0,diffY=-1;onBoard(x+diffX, y+diffY);--diffY)
                    {
                        if(isPiece(x+diffX, y+diffY))
                        {
                            if((board[coord(x+diffX,y+diffY)]&8) != (team<<3) && allowedMoves[coord(x+diffX, y+diffY)])
                            {
                                printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, 0);
                            }
                            break;
                        }
                        if(allowedMoves[coord(x+diffX, y+diffY)])
                            printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, blackCanCastleL, blackCanCastleR, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                    }
                    break;
                case KING:
                    if((team<<3) == WHITE)
                    {
                        wcl = 0;
                        wcr = 0;
                        if(whiteCanCastleL && tilesAreVoid(1, 3, 7) && tilesAreNotThreatened(2, 4, 7))
                        {
                            printMove(x, y, 2, 7, 0, -1, -1, -1, 0, 0, blackCanCastleL, blackCanCastleR, 0, 7, 3, 7, 0, 0, lastPawnMoveOrTake+1);
                        }
                        if(whiteCanCastleR && tilesAreVoid(5, 6, 7) && tilesAreNotThreatened(4, 6, 7))
                        {
                            printMove(x, y, 6, 7, 0, -1, -1, -1, 0, 0, blackCanCastleL, blackCanCastleR, 7, 7, 5, 7, 0, 0, lastPawnMoveOrTake+1);
                        }
                    }
                    else
                    {
                        bcl = 0;
                        bcr = 0;
                        if(blackCanCastleL && tilesAreVoid(1, 3, 0) && tilesAreNotThreatened(2, 4, 0))
                        {
                            printMove(x, y, 2, 0, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, 0, 0, 0, 0, 3, 0, 0, 0, lastPawnMoveOrTake+1);
                        }
                        if(blackCanCastleR && tilesAreVoid(5, 6, 0) && tilesAreNotThreatened(4, 6, 0))
                        {
                            printMove(x, y, 6, 0, 0, -1, -1, -1, whiteCanCastleL, whiteCanCastleR, 0, 0, 7, 0, 5, 0, 0, 0, lastPawnMoveOrTake+1);
                        }
                    }
                    for(diffX=-1;diffX<=1;++diffX)
                    {
                        for(diffY=-1;diffY<=1;++diffY)
                        {
                            if(diffX == 0 && diffY == 0)
                            {
                                continue;
                            }
                            if(onBoard(x+diffX, y+diffY))
                            {
                                if(board[coord(x+diffX, y+diffY)] == VOID && !isThreatened(x+diffX, y+diffY, 0))
                                {
                                    printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, lastPawnMoveOrTake+1);
                                }
                                else if((board[coord(x+diffX, y+diffY)]&8) != (team<<3) && !isThreatened(x+diffX, y+diffY, 0))
                                { // enemy
                                    printMove(x, y, x+diffX, y+diffY, 0, -1, -1, -1, wcl, wcr, bcl, bcr, -1, -1, -1, -1, 0, 0, 0);
                                }
                            }
                        }
                    }
                    break;
                default:
                    ;
            }
        }
    }
    
    if(possibleMoves == 0)
    {
        reinitRestrictedAndAllowedMoves();
        if(isEchec)
        { // chess mate
            printMove(-1, -1, -1, -1, 0, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1, turn==1, turn==0, lastPawnMoveOrTake+1);
        }
        else
        { // draw (pat)
            printMove(-1, -1, -1, -1, 0, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1, 1, 1, lastPawnMoveOrTake+1);
        }
    }
    
    std::cout << "end" << std::endl;
}

    std::cout << "end" << std::endl;
    
    return 0;
}