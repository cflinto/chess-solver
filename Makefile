

all:
	g++ -std=c++11 rules.cpp -o rules
	g++ -std=c++11 ponderator.cpp -o ponderate
	g++ -std=c++11 choose.cpp -o choose
	g++ -std=c++11 print_move.cpp -o print_move
	g++ -std=c++11 sum_ponderations.cpp -o sum_ponderations
	g++ -std=c++11 play_one.cpp -o play_one
	g++ -std=c++11 play_until_end.cpp -o play_until_end